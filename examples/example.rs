use key_vec::*;

macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}

fn main() {
  println!("key_vec example: main...");
  let mut v = KeyVec::<i32, char>::new();
  v.extend (vec![(10i32, 'a'), (-1, 'b'), (3, 'c'), (-1, 'd')].into_iter());
  show!(v);
  for x in v.drain(..) {
    show!(x);
  }
  println!("key_vec example: ...main");
}
