# `key_vec`

> Vec of key-value pairs sorted by key

[Documentation](https://docs.rs/key-vec)

```
let mut v = KeyVec::<i32, char>::new();
v.extend (vec![(10i32, 'a'), (-1, 'b'), (3, 'c'), (-1, 'd')].into_iter());
assert_eq!(*v, vec![(-1i32, 'd'), (3, 'c'), (10, 'a')]);
```
